﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Bar_Management
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            addBeerSum();
            addLiquorSum();
            addWineSum();
            showBeerList();
            showLiquorList();
            showWineList();
        }

        private void showWineList()
        {
            String cs2 = @"server=localhost;userid='root'; password='';database=mydb";
            MySqlConnection conn2 = null;
            conn2 = new MySqlConnection(cs2);
            conn2.Open();
            wineView.Items.Clear();
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn2;
            cmd.CommandText = "SELECT * FROM wineTable;";
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                var item = new ListViewItem();
                item.Text = reader["name"].ToString();        // 1st column text
                item.SubItems.Add(reader["type"].ToString());   //2nd column
                item.SubItems.Add(reader["year"].ToString());   //3rd column
                item.SubItems.Add(reader["quantity"].ToString());
                wineView.Items.Add(item);
            }
        }
        private void showLiquorList()
        {
            String cs2 = @"server=localhost;userid='root'; password='';database=mydb";
            MySqlConnection conn2 = null;
            conn2 = new MySqlConnection(cs2);
            conn2.Open();
            liqView.Items.Clear();
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn2;
            cmd.CommandText = "SELECT * FROM liquorTable;";
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                var item = new ListViewItem();
                item.Text = reader["name"].ToString();        // 1st column text
                item.SubItems.Add(reader["type"].ToString());  // 2nd column text
                item.SubItems.Add(reader["quantity"].ToString());
                liqView.Items.Add(item);
            }
        }
        private void showBeerList()
        {
            String cs2 = @"server=localhost;userid='root'; password='';database=mydb";
            MySqlConnection conn2 = null;
            conn2 = new MySqlConnection(cs2);
            conn2.Open();
            listView1.Items.Clear();
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn2;
            cmd.CommandText = "SELECT * FROM beerTable;";
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                var item = new ListViewItem();
                item.Text = reader["name"].ToString();        // 1st column text
                item.SubItems.Add(reader["quantity"].ToString());
                listView1.Items.Add(item);
            }

        }

        private void addBeerSum()
        {
            String cs2 = @"server=localhost;userid='root'; password='';database=mydb";
            MySqlConnection conn2 = null;

            object beerTotal;

            try
            {
                conn2 = new MySqlConnection(cs2);
                conn2.Open();

            }
            catch (Exception f)
            {
                Console.WriteLine("Error connecting");
            }

            MySqlCommand getBeerTotal = conn2.CreateCommand();
            getBeerTotal.CommandText = "Select sum(quantity) from beertable";

            beerTotal = getBeerTotal.ExecuteScalar();
            Console.WriteLine("Number of query is " + beerTotal);

            this.beerBox.Text += beerTotal;
            conn2.Close();
        }

        private void addLiquorSum(){

            object liquorTotal;

            String cs2 = @"server=localhost;userid='root'; password='';database=mydb";
            MySqlConnection conn2 = null;

            try
            {
                conn2 = new MySqlConnection(cs2);
                conn2.Open();

            }
            catch (Exception f)
            {
                Console.WriteLine("Error connecting");
            }

            MySqlCommand getLiquorTotal = conn2.CreateCommand();
            getLiquorTotal.CommandText = "Select sum(quantity) from liquortable";

            liquorTotal = getLiquorTotal.ExecuteScalar();

            liquorBox.Text += liquorTotal;

            conn2.Close();
        }

        private  void addWineSum(){

            String cs2 = @"server=localhost;userid='root'; password='';database=mydb";
            MySqlConnection conn2 = null;

            object wineTotal;


            try
            {
                conn2 = new MySqlConnection(cs2);
                conn2.Open();

            }
            catch (Exception f)
            {
                Console.WriteLine("Error connecting");
            }

            MySqlCommand getWineTotal = conn2.CreateCommand();
            getWineTotal.CommandText = "Select sum(quantity) from winetable";

            wineTotal = getWineTotal.ExecuteScalar();

            this.wineBox.Text += wineTotal;

            conn2.Close();
        }


        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //this.BringToFront();
            Application.Exit();

        }

        private void reportsButton_Click(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void tabPage3_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {

        }


        private void salesButton_Click(object sender, EventArgs e)
        {
            new Form6().ShowDialog();
        }

        private void addBeerButton_Click(object sender, EventArgs e)
        {
            Form3 addbeer = new Form3();
            addbeer.ShowDialog();

        }

        private void editBeerInvButton_Click(object sender, EventArgs e)
        {

        }

        private void beerBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void liquorBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void wineBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void beerTab_Click(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void addLiquorButton_Click(object sender, EventArgs e)
        {
            Form4 addliquor = new Form4();
            addliquor.ShowDialog();
        }

        private void addWineButton_Click(object sender, EventArgs e)
        {
            Form5 addwine = new Form5();
            addwine.ShowDialog();
        }

        private void editLiquorInvButton_Click(object sender, EventArgs e)
        {

        }

    }
}
