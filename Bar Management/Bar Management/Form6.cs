﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bar_Management
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();
            hideItems();
        }

        private void hideItems()
        {
            this.label3.Hide();
            this.comboBox2.Hide();
            this.wineLabel.Hide();
            this.comboBox3.Hide();
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if(this.comboBox1.SelectedItem.Equals("Beer"))
            {
                this.wineLabel.Hide();
                this.comboBox3.Hide();
                this.label3.Show();
                this.comboBox2.Show();
                
            }
            else if (this.comboBox1.SelectedItem.Equals("Wine"))
            {
                this.label3.Hide();
                this.comboBox2.Hide();
                this.wineLabel.Show();
                this.comboBox3.Show();
            }
            else
            {
                hideItems();
            }
        }

        private void wineLabel_Click(object sender, EventArgs e)
        {

        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            //run a query with the information


            this.comboBox1.Refresh();
            //this.label1.Click += new System.EventHandler(this.label1_Click);

        }
    }
}
