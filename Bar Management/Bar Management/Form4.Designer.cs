﻿namespace Bar_Management
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.nameLiquorTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.quantityLiquorTextbox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.typeLiquorComboBox = new System.Windows.Forms.ComboBox();
            this.addliquor_subbutton = new System.Windows.Forms.Button();
            this.liqcancelbutton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // nameLiquorTextBox
            // 
            this.nameLiquorTextBox.Location = new System.Drawing.Point(119, 30);
            this.nameLiquorTextBox.Name = "nameLiquorTextBox";
            this.nameLiquorTextBox.Size = new System.Drawing.Size(300, 20);
            this.nameLiquorTextBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Type";
            // 
            // quantityLiquorTextbox
            // 
            this.quantityLiquorTextbox.Location = new System.Drawing.Point(119, 86);
            this.quantityLiquorTextbox.Name = "quantityLiquorTextbox";
            this.quantityLiquorTextbox.Size = new System.Drawing.Size(300, 20);
            this.quantityLiquorTextbox.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Quantity";
            // 
            // typeLiquorComboBox
            // 
            this.typeLiquorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typeLiquorComboBox.FormattingEnabled = true;
            this.typeLiquorComboBox.Items.AddRange(new object[] {
            "Brand",
            "Gin",
            "Rum",
            "Tequila",
            "Vodka",
            "Whiskey",
            ""});
            this.typeLiquorComboBox.Location = new System.Drawing.Point(119, 59);
            this.typeLiquorComboBox.Name = "typeLiquorComboBox";
            this.typeLiquorComboBox.Size = new System.Drawing.Size(300, 21);
            this.typeLiquorComboBox.TabIndex = 6;
            // 
            // addliquor_subbutton
            // 
            this.addliquor_subbutton.Location = new System.Drawing.Point(173, 133);
            this.addliquor_subbutton.Name = "addliquor_subbutton";
            this.addliquor_subbutton.Size = new System.Drawing.Size(75, 23);
            this.addliquor_subbutton.TabIndex = 7;
            this.addliquor_subbutton.Text = "Add";
            this.addliquor_subbutton.UseVisualStyleBackColor = true;
            // 
            // liqcancelbutton
            // 
            this.liqcancelbutton.Location = new System.Drawing.Point(267, 133);
            this.liqcancelbutton.Name = "liqcancelbutton";
            this.liqcancelbutton.Size = new System.Drawing.Size(75, 23);
            this.liqcancelbutton.TabIndex = 8;
            this.liqcancelbutton.Text = "Cancel";
            this.liqcancelbutton.UseVisualStyleBackColor = true;
            this.liqcancelbutton.Click += new System.EventHandler(this.liqcancelbutton_Click);
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 182);
            this.Controls.Add(this.liqcancelbutton);
            this.Controls.Add(this.addliquor_subbutton);
            this.Controls.Add(this.typeLiquorComboBox);
            this.Controls.Add(this.quantityLiquorTextbox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nameLiquorTextBox);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(495, 221);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(495, 221);
            this.Name = "Form4";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add New Liquor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nameLiquorTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox quantityLiquorTextbox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox typeLiquorComboBox;
        private System.Windows.Forms.Button addliquor_subbutton;
        private System.Windows.Forms.Button liqcancelbutton;
    }
}