﻿namespace Bar_Management
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addbeer_subButton = new System.Windows.Forms.Button();
            this.cancelBeerButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.beernametextbox = new System.Windows.Forms.TextBox();
            this.beerqtytextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // addbeer_subButton
            // 
            this.addbeer_subButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.addbeer_subButton.Location = new System.Drawing.Point(160, 88);
            this.addbeer_subButton.Name = "addbeer_subButton";
            this.addbeer_subButton.Size = new System.Drawing.Size(96, 34);
            this.addbeer_subButton.TabIndex = 0;
            this.addbeer_subButton.Text = "Add";
            this.addbeer_subButton.UseVisualStyleBackColor = true;
            this.addbeer_subButton.Click += new System.EventHandler(this.addbeer_subButton_Click);
            // 
            // cancelBeerButton
            // 
            this.cancelBeerButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.cancelBeerButton.Location = new System.Drawing.Point(275, 88);
            this.cancelBeerButton.Name = "cancelBeerButton";
            this.cancelBeerButton.Size = new System.Drawing.Size(96, 34);
            this.cancelBeerButton.TabIndex = 1;
            this.cancelBeerButton.Text = "Cancel";
            this.cancelBeerButton.UseVisualStyleBackColor = true;
            this.cancelBeerButton.Click += new System.EventHandler(this.cancelBeerButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label1.Location = new System.Drawing.Point(48, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Name";
            // 
            // beernametextbox
            // 
            this.beernametextbox.Location = new System.Drawing.Point(112, 24);
            this.beernametextbox.Name = "beernametextbox";
            this.beernametextbox.Size = new System.Drawing.Size(304, 20);
            this.beernametextbox.TabIndex = 3;
            // 
            // beerqtytextbox
            // 
            this.beerqtytextbox.Location = new System.Drawing.Point(112, 50);
            this.beerqtytextbox.Name = "beerqtytextbox";
            this.beerqtytextbox.Size = new System.Drawing.Size(51, 20);
            this.beerqtytextbox.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label2.Location = new System.Drawing.Point(32, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Quantity";
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 154);
            this.Controls.Add(this.beerqtytextbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.beernametextbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancelBeerButton);
            this.Controls.Add(this.addbeer_subButton);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(488, 193);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(488, 193);
            this.Name = "Form3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add New Beer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addbeer_subButton;
        private System.Windows.Forms.Button cancelBeerButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox beernametextbox;
        private System.Windows.Forms.TextBox beerqtytextbox;
        private System.Windows.Forms.Label label2;
    }
}