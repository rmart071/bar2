﻿namespace Bar_Management
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Summary = new System.Windows.Forms.TabControl();
            this.summaryTab = new System.Windows.Forms.TabPage();
            this.reportsButton = new System.Windows.Forms.Button();
            this.salesButton = new System.Windows.Forms.Button();
            this.wineBox = new System.Windows.Forms.TextBox();
            this.liquorBox = new System.Windows.Forms.TextBox();
            this.beerBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.welcome = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.beerTab = new System.Windows.Forms.TabPage();
            this.listView1 = new System.Windows.Forms.ListView();
            this.Column1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Quantity = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.editBeerInvButton = new System.Windows.Forms.Button();
            this.addBeerButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.liquorTab = new System.Windows.Forms.TabPage();
            this.liqView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.editLiquorInvButton = new System.Windows.Forms.Button();
            this.addLiquorButton = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.wineTab = new System.Windows.Forms.TabPage();
            this.wineView = new System.Windows.Forms.ListView();
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.editWineInvButton = new System.Windows.Forms.Button();
            this.addWineButton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.Summary.SuspendLayout();
            this.summaryTab.SuspendLayout();
            this.beerTab.SuspendLayout();
            this.liquorTab.SuspendLayout();
            this.wineTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // Summary
            // 
            this.Summary.Controls.Add(this.summaryTab);
            this.Summary.Controls.Add(this.beerTab);
            this.Summary.Controls.Add(this.liquorTab);
            this.Summary.Controls.Add(this.wineTab);
            this.Summary.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Summary.ItemSize = new System.Drawing.Size(50, 25);
            this.Summary.Location = new System.Drawing.Point(0, 0);
            this.Summary.Name = "Summary";
            this.Summary.SelectedIndex = 0;
            this.Summary.Size = new System.Drawing.Size(827, 507);
            this.Summary.TabIndex = 0;
            // 
            // summaryTab
            // 
            this.summaryTab.Controls.Add(this.reportsButton);
            this.summaryTab.Controls.Add(this.salesButton);
            this.summaryTab.Controls.Add(this.wineBox);
            this.summaryTab.Controls.Add(this.liquorBox);
            this.summaryTab.Controls.Add(this.beerBox);
            this.summaryTab.Controls.Add(this.label6);
            this.summaryTab.Controls.Add(this.label5);
            this.summaryTab.Controls.Add(this.label4);
            this.summaryTab.Controls.Add(this.welcome);
            this.summaryTab.Controls.Add(this.label1);
            this.summaryTab.Location = new System.Drawing.Point(4, 29);
            this.summaryTab.Name = "summaryTab";
            this.summaryTab.Padding = new System.Windows.Forms.Padding(3);
            this.summaryTab.Size = new System.Drawing.Size(819, 474);
            this.summaryTab.TabIndex = 0;
            this.summaryTab.Text = "Summary";
            this.summaryTab.UseVisualStyleBackColor = true;
            this.summaryTab.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // reportsButton
            // 
            this.reportsButton.Location = new System.Drawing.Point(455, 365);
            this.reportsButton.Name = "reportsButton";
            this.reportsButton.Size = new System.Drawing.Size(129, 34);
            this.reportsButton.TabIndex = 10;
            this.reportsButton.Text = "Reports";
            this.reportsButton.UseVisualStyleBackColor = true;
            this.reportsButton.Click += new System.EventHandler(this.reportsButton_Click);
            // 
            // salesButton
            // 
            this.salesButton.Location = new System.Drawing.Point(259, 365);
            this.salesButton.Name = "salesButton";
            this.salesButton.Size = new System.Drawing.Size(129, 34);
            this.salesButton.TabIndex = 9;
            this.salesButton.Text = "Enter Sales";
            this.salesButton.UseVisualStyleBackColor = true;
            this.salesButton.Click += new System.EventHandler(this.salesButton_Click);
            // 
            // wineBox
            // 
            this.wineBox.Location = new System.Drawing.Point(469, 240);
            this.wineBox.Name = "wineBox";
            this.wineBox.ReadOnly = true;
            this.wineBox.Size = new System.Drawing.Size(62, 26);
            this.wineBox.TabIndex = 8;
            this.wineBox.TextChanged += new System.EventHandler(this.wineBox_TextChanged);
            // 
            // liquorBox
            // 
            this.liquorBox.Location = new System.Drawing.Point(469, 187);
            this.liquorBox.Name = "liquorBox";
            this.liquorBox.ReadOnly = true;
            this.liquorBox.Size = new System.Drawing.Size(62, 26);
            this.liquorBox.TabIndex = 7;
            this.liquorBox.TextChanged += new System.EventHandler(this.liquorBox_TextChanged);
            // 
            // beerBox
            // 
            this.beerBox.Location = new System.Drawing.Point(469, 135);
            this.beerBox.Name = "beerBox";
            this.beerBox.ReadOnly = true;
            this.beerBox.Size = new System.Drawing.Size(62, 26);
            this.beerBox.TabIndex = 6;
            this.beerBox.TextChanged += new System.EventHandler(this.beerBox_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Underline);
            this.label6.Location = new System.Drawing.Point(289, 240);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(150, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Total Number of WIne:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Underline);
            this.label5.Location = new System.Drawing.Point(289, 193);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(158, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Total Number of Liquor:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Underline);
            this.label4.Location = new System.Drawing.Point(289, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(155, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Total Number of Beers:";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // welcome
            // 
            this.welcome.AutoSize = true;
            this.welcome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.welcome.Location = new System.Drawing.Point(19, 23);
            this.welcome.Name = "welcome";
            this.welcome.Size = new System.Drawing.Size(125, 13);
            this.welcome.TabIndex = 1;
            this.welcome.Text = "Welcome, (name of user)";
            this.welcome.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(360, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Summary";
            this.label1.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // beerTab
            // 
            this.beerTab.Controls.Add(this.listView1);
            this.beerTab.Controls.Add(this.editBeerInvButton);
            this.beerTab.Controls.Add(this.addBeerButton);
            this.beerTab.Controls.Add(this.label2);
            this.beerTab.Location = new System.Drawing.Point(4, 29);
            this.beerTab.Name = "beerTab";
            this.beerTab.Padding = new System.Windows.Forms.Padding(3);
            this.beerTab.Size = new System.Drawing.Size(819, 474);
            this.beerTab.TabIndex = 1;
            this.beerTab.Text = "Beer";
            this.beerTab.UseVisualStyleBackColor = true;
            this.beerTab.Click += new System.EventHandler(this.beerTab_Click);
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Column1,
            this.Quantity});
            this.listView1.Location = new System.Drawing.Point(55, 83);
            this.listView1.MaximumSize = new System.Drawing.Size(726, 306);
            this.listView1.MinimumSize = new System.Drawing.Size(726, 306);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(726, 306);
            this.listView1.TabIndex = 3;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // Column1
            // 
            this.Column1.Text = "Name";
            this.Column1.Width = 350;
            // 
            // Quantity
            // 
            this.Quantity.Text = "Quantity";
            this.Quantity.Width = 350;
            // 
            // editBeerInvButton
            // 
            this.editBeerInvButton.Location = new System.Drawing.Point(443, 430);
            this.editBeerInvButton.Name = "editBeerInvButton";
            this.editBeerInvButton.Size = new System.Drawing.Size(228, 32);
            this.editBeerInvButton.TabIndex = 2;
            this.editBeerInvButton.Text = "Edit Beer Inventory";
            this.editBeerInvButton.UseVisualStyleBackColor = true;
            this.editBeerInvButton.Click += new System.EventHandler(this.editBeerInvButton_Click);
            // 
            // addBeerButton
            // 
            this.addBeerButton.Location = new System.Drawing.Point(150, 430);
            this.addBeerButton.Name = "addBeerButton";
            this.addBeerButton.Size = new System.Drawing.Size(228, 32);
            this.addBeerButton.TabIndex = 1;
            this.addBeerButton.Text = "Add New Beer to Inventory";
            this.addBeerButton.UseVisualStyleBackColor = true;
            this.addBeerButton.Click += new System.EventHandler(this.addBeerButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(382, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "Beer";
            // 
            // liquorTab
            // 
            this.liquorTab.Controls.Add(this.liqView);
            this.liquorTab.Controls.Add(this.editLiquorInvButton);
            this.liquorTab.Controls.Add(this.addLiquorButton);
            this.liquorTab.Controls.Add(this.label7);
            this.liquorTab.Location = new System.Drawing.Point(4, 29);
            this.liquorTab.Name = "liquorTab";
            this.liquorTab.Padding = new System.Windows.Forms.Padding(3);
            this.liquorTab.Size = new System.Drawing.Size(819, 474);
            this.liquorTab.TabIndex = 2;
            this.liquorTab.Text = "Liquor";
            this.liquorTab.UseVisualStyleBackColor = true;
            this.liquorTab.Click += new System.EventHandler(this.tabPage3_Click);
            // 
            // liqView
            // 
            this.liqView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.liqView.Location = new System.Drawing.Point(47, 83);
            this.liqView.Name = "liqView";
            this.liqView.Size = new System.Drawing.Size(728, 316);
            this.liqView.TabIndex = 5;
            this.liqView.UseCompatibleStateImageBehavior = false;
            this.liqView.View = System.Windows.Forms.View.Details;
            this.liqView.SelectedIndexChanged += new System.EventHandler(this.listView2_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 300;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Type";
            this.columnHeader2.Width = 200;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Quantity";
            this.columnHeader3.Width = 200;
            // 
            // editLiquorInvButton
            // 
            this.editLiquorInvButton.Location = new System.Drawing.Point(443, 430);
            this.editLiquorInvButton.Name = "editLiquorInvButton";
            this.editLiquorInvButton.Size = new System.Drawing.Size(228, 32);
            this.editLiquorInvButton.TabIndex = 4;
            this.editLiquorInvButton.Text = "Edit Liquor Inventory";
            this.editLiquorInvButton.UseVisualStyleBackColor = true;
            this.editLiquorInvButton.Click += new System.EventHandler(this.editLiquorInvButton_Click);
            // 
            // addLiquorButton
            // 
            this.addLiquorButton.Location = new System.Drawing.Point(150, 430);
            this.addLiquorButton.Name = "addLiquorButton";
            this.addLiquorButton.Size = new System.Drawing.Size(228, 32);
            this.addLiquorButton.TabIndex = 3;
            this.addLiquorButton.Text = "Add New Liquor to Inventory";
            this.addLiquorButton.UseVisualStyleBackColor = true;
            this.addLiquorButton.Click += new System.EventHandler(this.addLiquorButton_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(362, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 25);
            this.label7.TabIndex = 1;
            this.label7.Text = "Liquor";
            // 
            // wineTab
            // 
            this.wineTab.Controls.Add(this.wineView);
            this.wineTab.Controls.Add(this.editWineInvButton);
            this.wineTab.Controls.Add(this.addWineButton);
            this.wineTab.Controls.Add(this.label8);
            this.wineTab.Location = new System.Drawing.Point(4, 29);
            this.wineTab.Name = "wineTab";
            this.wineTab.Padding = new System.Windows.Forms.Padding(3);
            this.wineTab.Size = new System.Drawing.Size(819, 474);
            this.wineTab.TabIndex = 3;
            this.wineTab.Text = "Wine";
            this.wineTab.UseVisualStyleBackColor = true;
            // 
            // wineView
            // 
            this.wineView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7});
            this.wineView.Location = new System.Drawing.Point(39, 68);
            this.wineView.Name = "wineView";
            this.wineView.Size = new System.Drawing.Size(740, 340);
            this.wineView.TabIndex = 5;
            this.wineView.UseCompatibleStateImageBehavior = false;
            this.wineView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Name";
            this.columnHeader4.Width = 300;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Year";
            this.columnHeader5.Width = 100;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Type";
            this.columnHeader6.Width = 100;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Quantity";
            this.columnHeader7.Width = 200;
            // 
            // editWineInvButton
            // 
            this.editWineInvButton.Location = new System.Drawing.Point(443, 430);
            this.editWineInvButton.Name = "editWineInvButton";
            this.editWineInvButton.Size = new System.Drawing.Size(228, 32);
            this.editWineInvButton.TabIndex = 4;
            this.editWineInvButton.Text = "Edit Wine Inventory";
            this.editWineInvButton.UseVisualStyleBackColor = true;
            // 
            // addWineButton
            // 
            this.addWineButton.Location = new System.Drawing.Point(150, 430);
            this.addWineButton.Name = "addWineButton";
            this.addWineButton.Size = new System.Drawing.Size(228, 32);
            this.addWineButton.TabIndex = 3;
            this.addWineButton.Text = "Add New Wine to Inventory";
            this.addWineButton.UseVisualStyleBackColor = true;
            this.addWineButton.Click += new System.EventHandler(this.addWineButton_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(362, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 25);
            this.label8.TabIndex = 1;
            this.label8.Text = "Wine";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.button1.Location = new System.Drawing.Point(728, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(86, 25);
            this.button1.TabIndex = 9;
            this.button1.Text = "Logout";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 503);
            this.ControlBox = false;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Summary);
            this.MaximumSize = new System.Drawing.Size(843, 542);
            this.MinimumSize = new System.Drawing.Size(843, 542);
            this.Name = "Form2";
            this.Text = "Name of App";
            this.Summary.ResumeLayout(false);
            this.summaryTab.ResumeLayout(false);
            this.summaryTab.PerformLayout();
            this.beerTab.ResumeLayout(false);
            this.beerTab.PerformLayout();
            this.liquorTab.ResumeLayout(false);
            this.liquorTab.PerformLayout();
            this.wineTab.ResumeLayout(false);
            this.wineTab.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl Summary;
        private System.Windows.Forms.TabPage summaryTab;
        private System.Windows.Forms.TabPage beerTab;
        private System.Windows.Forms.TabPage liquorTab;
        private System.Windows.Forms.TabPage wineTab;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox wineBox;
        private System.Windows.Forms.TextBox liquorBox;
        private System.Windows.Forms.TextBox beerBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button reportsButton;
        private System.Windows.Forms.Button salesButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label welcome;
        private System.Windows.Forms.Button editBeerInvButton;
        private System.Windows.Forms.Button addBeerButton;
        private System.Windows.Forms.Button editLiquorInvButton;
        private System.Windows.Forms.Button addLiquorButton;
        private System.Windows.Forms.Button editWineInvButton;
        private System.Windows.Forms.Button addWineButton;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader Column1;
        private System.Windows.Forms.ColumnHeader Quantity;
        private System.Windows.Forms.ListView liqView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ListView wineView;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;

    }
}