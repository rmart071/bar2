﻿namespace Bar_Management
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.liqcancelbutton = new System.Windows.Forms.Button();
            this.addliquor_subbutton = new System.Windows.Forms.Button();
            this.typeWineComboBox = new System.Windows.Forms.ComboBox();
            this.quantityWineTextbox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nameWineTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.yearWineComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // liqcancelbutton
            // 
            this.liqcancelbutton.Location = new System.Drawing.Point(250, 155);
            this.liqcancelbutton.MaximumSize = new System.Drawing.Size(75, 23);
            this.liqcancelbutton.MinimumSize = new System.Drawing.Size(75, 23);
            this.liqcancelbutton.Name = "liqcancelbutton";
            this.liqcancelbutton.Size = new System.Drawing.Size(75, 23);
            this.liqcancelbutton.TabIndex = 16;
            this.liqcancelbutton.Text = "Cancel";
            this.liqcancelbutton.UseVisualStyleBackColor = true;
            this.liqcancelbutton.Click += new System.EventHandler(this.liqcancelbutton_Click);
            // 
            // addliquor_subbutton
            // 
            this.addliquor_subbutton.Location = new System.Drawing.Point(156, 155);
            this.addliquor_subbutton.Name = "addliquor_subbutton";
            this.addliquor_subbutton.Size = new System.Drawing.Size(75, 23);
            this.addliquor_subbutton.TabIndex = 15;
            this.addliquor_subbutton.Text = "Add";
            this.addliquor_subbutton.UseVisualStyleBackColor = true;
            // 
            // typeWineComboBox
            // 
            this.typeWineComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typeWineComboBox.FormattingEnabled = true;
            this.typeWineComboBox.Items.AddRange(new object[] {
            "Red",
            "White"});
            this.typeWineComboBox.Location = new System.Drawing.Point(102, 81);
            this.typeWineComboBox.Name = "typeWineComboBox";
            this.typeWineComboBox.Size = new System.Drawing.Size(300, 21);
            this.typeWineComboBox.TabIndex = 14;
            // 
            // quantityWineTextbox
            // 
            this.quantityWineTextbox.Location = new System.Drawing.Point(102, 108);
            this.quantityWineTextbox.Name = "quantityWineTextbox";
            this.quantityWineTextbox.Size = new System.Drawing.Size(300, 20);
            this.quantityWineTextbox.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Quantity";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Type";
            // 
            // nameWineTextBox
            // 
            this.nameWineTextBox.Location = new System.Drawing.Point(102, 25);
            this.nameWineTextBox.Name = "nameWineTextBox";
            this.nameWineTextBox.Size = new System.Drawing.Size(300, 20);
            this.nameWineTextBox.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Name";
            // 
            // yearWineComboBox
            // 
            this.yearWineComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.yearWineComboBox.FormattingEnabled = true;
            this.yearWineComboBox.Items.AddRange(new object[] {
            "1930",
            "1931",
            "1932",
            "1933",
            "1934",
            "1935",
            "1936",
            "1937",
            "1938",
            "1939",
            "1940",
            "1941",
            "1942",
            "1943",
            "1944",
            "1945",
            "1946",
            "1947",
            "1948",
            "1949",
            "1950",
            "1951",
            "1952",
            "1953",
            "1954",
            "1955",
            "1956",
            "1957",
            "1958",
            "1959",
            "1960",
            "1961",
            "1962",
            "1963",
            "1964",
            "1965",
            "1966",
            "1967",
            "1968",
            "1969",
            "1970",
            "1971",
            "1972",
            "1973",
            "1974",
            "1975",
            "1976",
            "1977",
            "1978",
            "1979",
            "1980",
            "1981",
            "1982",
            "1983",
            "1984",
            "1985",
            "1986",
            "1987",
            "1988",
            "1989",
            "1990",
            "1991",
            "1992",
            "1993",
            "1994",
            "1995",
            "1996",
            "1997",
            "1998",
            "1999",
            "2000",
            "2001",
            "2002",
            "2003",
            "2004",
            "2005",
            "2006",
            "2007",
            "2008",
            "2009",
            "2010",
            "2011",
            "2012",
            "2013",
            "2014"});
            this.yearWineComboBox.Location = new System.Drawing.Point(102, 53);
            this.yearWineComboBox.Name = "yearWineComboBox";
            this.yearWineComboBox.Size = new System.Drawing.Size(300, 21);
            this.yearWineComboBox.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "year";
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 203);
            this.Controls.Add(this.yearWineComboBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.liqcancelbutton);
            this.Controls.Add(this.addliquor_subbutton);
            this.Controls.Add(this.typeWineComboBox);
            this.Controls.Add(this.quantityWineTextbox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nameWineTextBox);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(484, 242);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(484, 242);
            this.Name = "Form5";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add New Wine";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button liqcancelbutton;
        private System.Windows.Forms.Button addliquor_subbutton;
        private System.Windows.Forms.ComboBox typeWineComboBox;
        private System.Windows.Forms.TextBox quantityWineTextbox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox nameWineTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox yearWineComboBox;
        private System.Windows.Forms.Label label4;
    }
}